###############################
##
## Dockerfile for the homalg release
##
## Creates a container with arch linux, Polymake, Singular, GAP, and homalg packages as git repositories.
##
###############################

FROM sebasguts/arch_for_homalg

MAINTAINER Sebastian Gutsche

ADD install_files /install_files

RUN \
    ## Install polymake
    cd /tmp && \
    git clone --branch Snapshots https://github.com/polymake/polymake.git polymake-beta && \
    cd polymake-beta && \
    ./configure --with-gmp=system --without-java && make -j && sudo make install && \
    cd .. && rm -rf polymake-beta && \
    ## Install singular
    wget https://aur.archlinux.org/packages/si/singular-git/singular-git.tar.gz && tar -xf singular-git.tar.gz && \
    rm singular-git.tar.gz && cd /tmp/singular-git && sudo pacman-db-upgrade && makepkg -i --noconfirm && \
    rm -rf /tmp/singular-git && \
    ## Install gap (crucial)
    cd /tmp && wget http://www.gap-system.org/pub/gap/gap47/tar.gz/gap4r7p7_2015_02_13-15_29.tar.gz && tar -xf gap*tar* && rm -rf gap*tar* && \
    sudo mv gap4r7 /opt/ && sudo chown -R homalg /opt/gap4r7 && \
    cd /opt/gap4r7 && ./configure --with-gmp=system && make -j && \
    cd pkg/io-4.4.4/ && ./configure && make && \
    cd ../Browse && ./configure && make && \
    cd ../orb-4.7.3/ && ./configure && make && cd .. && \
    rm -rf 4ti2Interface/ AutoDoc/ Convex/ ExamplesForHomalg/ Gauss GaussForHomalg/ GradedModules/ GradedRingForHomalg/ homalg/ HomalgToCAS/ IO_ForHomalg/ LocalizeRingForHomalg/ MatricesForHomalg/ PolymakeInterface/ RingsForHomalg/ SCO/ ToolsForHomalg/ ToricVarieties/ && \
    cd .. && mkdir local && cd local && mkdir pkg && cd pkg && \
    cp /install_files/pull_packages . && ./pull_packages && \
    sudo ln -snf /opt/gap4r7 /opt/gap && \
    cd ../.. && cp /install_files/create_workspace . && ./create_workspace && \
    sudo bash -c "echo '/opt/gap4r7/bin/gap.sh -l \"/opt/gap4r7/local;/opt/gap4r7\" \"\$@\"' > /usr/bin/gap" && \
    sudo bash -c "echo '/opt/gap4r7/bin/gap.sh -l \"/opt/gap4r7/local;/opt/gap4r7\" -L /opt/gap4r7/bin/wsgap4 \"\$@\"' > /usr/bin/gapL" && \
    sudo chmod +x /usr/bin/gap && \
    sudo chmod +x /usr/bin/gapL && \
    mkdir /home/homalg/.gap && \
    cp /install_files/gap.ini /home/homalg/.gap/ 

ENV HOME /home/homalg

WORKDIR /home/homalg

ENTRYPOINT /bin/bash
